My config for sway and sway-related things.

Dependencies (these are all Debian packages):

  * sway
  * swayidle
  * swaylock
  * waybar
  * tofi

This includes waybar config, which needs to be symlinked:

`ln -s ~/.config/sway/waybar ~/.config/waybar`
