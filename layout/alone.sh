#!/bin/sh
swaymsg \
    output eDP-1 mode 1920x1080@60Hz scale 1 pos 0 0 transform 0, \
    output DP-4 disable, \
    output DP-1 disable, \
    output HDMI-A-1 disable
