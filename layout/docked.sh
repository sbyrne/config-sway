#!/bin/sh
swaymsg \
    output eDP-1 disable, \
    output DP-4 disable, \
    output DP-1     mode 3840x2160@60Hz scale 1.5 pos 1440 1120 transform 0, \
    output HDMI-A-1 mode 3840x2160@60Hz scale 1.5 pos 0    0    transform 270,
