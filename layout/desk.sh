#!/bin/sh
swaymsg \
    output DP-4 mode 1920x1200@60Hz scale 1 pos 0 0 transform 0, \
    output eDP-1 disable, \
    output DP-1 disable, \
    output HDMI-A-1 disable,
